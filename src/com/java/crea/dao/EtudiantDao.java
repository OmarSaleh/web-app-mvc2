package com.java.crea.dao;

import java.sql.SQLException;

import com.java.crea.models.EtudiantBean;
import com.java.crea.utils.DBAction;


public class EtudiantDao {

	public static int addStudent(EtudiantBean add_student) {
		int result = -1;
		
		DBAction.DBConnexion();
		
		String req = "INSERT INTO etudiant (nom, prenom, classe, note)" + " VALUES ('" + add_student.getNom() + "','" + add_student.getPrenom() + "','" + add_student.getClasse() + "'," + add_student.getNote() + ") ";
		try {
			result = DBAction.getStm().executeUpdate(req);
		} catch (SQLException ex) {
			if (ex.getErrorCode() == 1062) {
				result = -2;
			}
			System.out.println(ex.getMessage());
		}
		DBAction.DBClose();
		
		return result;		
	}
}


