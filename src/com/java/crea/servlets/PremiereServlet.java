package com.java.crea.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.java.crea.dao.EtudiantDao;
import com.java.crea.models.EtudiantBean;

/**
 * Servlet implementation class PremiereServlet
 */
@WebServlet("/PremiereServlet")
public class PremiereServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PremiereServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

		EtudiantBean etudiant = new EtudiantBean("benj", "synda","DEV3",10);
		
		request.setAttribute("etudiant", etudiant);
		
	     String nom = request.getParameter("nom");
	     String prenom = request.getParameter("prenom");
	     String classe = request.getParameter("classe");
		 String note = request.getParameter("note");
	        
	     HttpSession session = request.getSession();

	     session.setAttribute("nom", nom);
	     session.setAttribute("prenom", prenom);
	     session.setAttribute("classe", classe);
	     session.setAttribute("note", note);


	    this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
	  String nom = request.getParameter("nom");
	  String prenom = request.getParameter("prenom");
	  String classe = request.getParameter("classe");
	  Integer note = Integer.parseInt(request.getParameter("note"));
      request.setAttribute("nom", nom);
      request.setAttribute("prenom", prenom);
      request.setAttribute("classe", classe);
      request.setAttribute("note", note);
      
      EtudiantBean etudiant = new EtudiantBean(nom, prenom, classe, note);
      EtudiantDao.addStudent(etudiant);
      
      this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

	}

}
