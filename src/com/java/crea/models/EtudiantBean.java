package com.java.crea.models;

public class EtudiantBean {
	
	private String nom;
	private String prenom;
	private String classe;
	private int note;
	
	
	public EtudiantBean(String nom, String prenom, String classe, int note) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.classe = classe;
		this.note = note;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	

}
